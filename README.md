Mon nouveau site web pour la NSI avec MkDocs et Pyodide.

- Lien vers le [site en ligne](https://vincent_jarc.gitlab.io/cours/). 
- Tutoriel MkDocs : La documentation est sur https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review
- L'adresse originale du rendu de ce site modèle avant clonage est : [Modèle de projet avec Python](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/)



