---
title: Module vlc
author: M.Jarc
tags:
    - VLC
    - Python
    - Bonus
    - Musique
---

# Le module `vlc` ![code](../pics/vlc64.png)

## La bibliothèque VLC

Vous connaissez sûrement le logiciel français VLC Media Player ? Libre et multiplateforme, celui-ci permet de lire de nombreux formats audios ou vidéos sans avoir à installer de codecs. Une bibliothèque python permet également d'utiliser les fonctionnalités du logiciel dans vos scripts python. Pour l'utiliser, vous devez avoir déjà installé VLC Media Player sur votre ordinateur et installer le module `python-vlc` avec la commande `pip`.

## Jouer un son

Pour jouer un son avec VLC, rien de plus simple : vous devez créer un lecteur et le démarrer. 

!!! example "Exemple"

	```python linenums='1'
	from vlc import MediaPlayer

    lecteur = MediaPlayer("un/nom/de/fichier")
    lecteur.play()
	```

Le lecteur peut également être arrêté grâce à `player.stop()` ou mis en pause avec `player.pause()`. Cela peut bien sûr être couplé avec une interface Tkinter pour plus de souplesse et vous pouvez créer autant de lecteurs que vous le souhaitez (qui peuvent même lire des morceaux différents en même temps). 

## Jouer une playliste

La méthode précédente est suffisante pour jouer un son unique, mais pour jouer une liste complète de morceaux, la technique est plus complexe. Tout d'abord, vous devez créer une instance VLC (une sorte de logiciel virtuel) puis, à partir de l'instance, créer une liste de médias et un lecteur. 

!!! example "Exemple"

    ```python linenums='1'
    from vlc import Instance

    instance = vlc.Instance()
    liste    = instance.media_list_new()
    lecteur  = instance.media_list_player_new()
    ```

Une fois ces trois éléments créés, vous pouvez ajouter autant de médias à votre liste que vous le souhaitez.

!!! example "Exemples"

    ```python linenums='6'
    media1 = instance.media_new("morceau_1.mp3")
    media2 = instance.media_new("morceau_2.ogg")
    liste.add_media(media1)
    liste.add_media(media2)
    ```
    
    ou 

    ```python linenums='6'
    liste.add_media(instance.media_new("morceau_1.mp3"))
    liste.add_media(instance.media_new("morceau_2.ogg"))
    ```
    
    ou 

    ```python linenums='6'
    noms_morceaux = ["morceau_1.mp3", "morceau_2.ogg"]
    for morceau in noms_morceaux:
        liste.add_media(instance.media_new(morceau))
    ```

Dernière étape, il ne vous reste plus qu'à lier votre liste de médias au lecteur. 

!!! example "Exemple"

    ```python linenums='10'
    lecteur.set_media_list(liste)
    ```

Ne reste plus qu'à lancer le lecteur avec `lecteur.play()`, comme avant. Vous pouvez même obliger votre lecteur à jouer en boucle. 

!!! example "Exemple"

    ```python linenums='11'
    from vlc import PlaybackMode

    lecteur.set_playback_mode(PlaybackMode.loop)
    ```