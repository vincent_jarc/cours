---
title: Module json
author: M.Jarc
tags:
    - JSON
    - Python
    - Bonus
---

# Le module `json` ![code](../pics/json64.png)

## Principe et syntaxe du langage JSON

Un fichier JSON est un simple fichier texte portant l'extension `.json` qui vous permettra de stocker, enregistrer et récupérer très facilement vos données. Sa syntaxe doit cependant répondre à la syntaxe du langage Javascript ; en effet JSON signifie *Java Script Object Notation*. Cette syntaxe est très proche de celle de Python, mais voici toutefois quelques différences notables :

- Les chaînes de caractères se notent entre des doubles guillemets, **jamais de simples guillemets !** Exemple : `txt = "Bonjour"` mais jamais `txt = 'bonjour'`. 
- Les booléens se notent en minuscule. Exemple : `ok = true` ou `ok = false`. 
- La valeur `None` se note `null`. 
- JSON ne connaît pas les `tuple`, il faudra utiliser soit des `list` (appelés `array` en JSON), soit des `dict` (appelé `object` en JSON).
- JSON n'accepte pas les commentaires, même en respectant la syntaxe de Javascript. 

## Enregistrer vos données

Les fichiers JSON vont vous permettre d'enregistrer vos tableaux et vos dictionnaires très facilement. Pour cela, vous aurez besoin de la bibliothèque `json`. Le principe est simple : 

1. ouvrez votre fichier en mode écriture avec la fonction `open()` de Python, 
2. enregistrez vos données avec la fonction `dump` de la bibliothèque `json`,
3. enfin, fermez votre fichier. 

!!! example "Exemple"

    ```python linenums='1'
    import json

    monTableau = [11,2,25,32]

    fichier = open(file="essai1.bonus", mode="w") #Étape 1 : w = write
    json.dump(monTableau, fichier) 			     #Étape 2
    fichier.close() 							 #Étape 3
    ```

Résultat : `[11, 2, 25, 32]`

Deuxième exemple avec un dictionnaire. Le principe est le même, la seule différence, c'est que nous aimerions qu'il soit correctement présenté dans le fichier JSON pour pouvoir (au besoin), le modifier nous-même avec un éditeur de texte comme Notepad++. 

!!! example "Exemple"

    ```python linenums='1'
    import json

    monDico = {"nombres":[1,2,3], "or":5, "nom":"bidule", "alive":True}

    fichier = open(file="essai2.bonus", mode="w") #Étape 1
    json.dump(monDico, fichier, indent=4) 		 #Étape 2 : on règle l'indentation à 4 espaces
    fichier.close() 							 #Étape 3
    ```

Résultat : 

```json linenums='1'
{
    "nombres": [
        1,
        2,
        3
    ],
    "or": 5,
    "nom": "bidule",
    "alive": true
}
```

Attention, la fonction `dump()` n'acceptera pas que vous lui donniez vos classes personnelles à enregistrer ! Si vous souhaitez enregistrer un objet, il faudra auparavant le transformer en dictionnaire. 

## Charger vos données

La récupération des données se fait de façon très similaire. Deux différences toutefois : 

1. le fichier doit être ouvert en mode lecture et non écriture ; 
2. il faudra utiliser la fonction `load()` du module `json` à la place de `dump()`

!!! example "Exemple"

    ```python linenums='1'
    import json

    fichier    = open(file="essai1.bonus", mode="r") #Étape 1
    mesDonnees = json.load(fichier) 				#Étape 2
    fichier.close() 								#Étape 3

    print(mesDonnees)
    ```

Vous remarquerez que la fermeture du fichier est absolument nécessaire ! Un fichier non (ou mal) fermé pourrait être impossible à ouvrir. Pour éviter les oublis ou qu'une erreur empêche la fermeture du fichier, vous pouvez remplacer le couple `open()` - `close()` par un bloc `with`-`as` : 

!!! example "Exemple"

    ```python linenums='1'
    import json

    with open(file="essai1.bonus", mode="r") as fichier:
        mesDonnees = json.load(fichier)

    print(mesDonnees)
    ```