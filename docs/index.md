---
author: M.JARC
---

![bandeau](pics/logo.png)

# Site de la spécialité NSI du lycée Jean Moulin de Montmorilon 

Sur ce site, vous trouverez les diverses informations nécessaires à la réalisation de votre projet : objectif du projet et des différentes phases, organisation de votre code, organisation du groupe, utilisation de Tkinter…