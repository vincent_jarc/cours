---
title: "Accueil Terminale"
author: M.Jarc
tags:
    - Term
---

![bandeau](../pics/logo.png)

# Progression

1. [Dictionnaires & API](01_API.md)
2. Récursivité
3. Programmation orientée objet
4. Architecture logicielle : les processus
5. Piles et files
6. Routage
7. Programmation dynamique
8. Bases de données
9. Arbres
10. Cryptographie
11. Systèmes sur puce
12. Programmation fonctionnelle
13. Graphes
14. Calculabilité et décidabilité
15. Algoithme de Boyer-Moore-Horspool

# Evaluation

L'évaluation de NSI se fera de la manière suivante : 

- Devoirs express sur 10 points toutes les 2 semaines : 2 à 3 sujets de la banque officielle des épreuves pratiques de l'année précédente seront donnés à travailler. Deux exercices parmi ces sujets devront être restitués (avec d'éventuelles modifications)
- Devoirs théorique sur 10 points : donnés après un ou deux chapitres, à raison d'*environ* 3 par trimestre. 
- Projet : réalisé par groupe durant toute l'année, celui-ci sera évalué à intervalles réguliers (environ 4 semaines) pour corriger l'avancement. Des cahiers des charges spécifiques seront donnés en cours. Une séance de hackathon sera proposée une semaine sur deux suite aux devoirs express. 

# Le baccalauréat

Les épreuves du bac de NSI se passent en 2 temps : 

- une **épreuve théorique** de 3H30 comptant pour 12 points. Elle consiste à résoudre 3 exercices portant sur divers thèmes étudiés durant les deux années de NSI ; 
- une **épreuve pratique** de 1H comptant pour 8 points. Elle consiste à rédiger deux programmes : le premier est un "*classique*" vu pendant l'année, le second un programme à trou. 

Ces deux épreuves donnent une note sur 20 qui comptera coefficient 16. A cela s'ajoute l'épreuve du **Grand Oral**. Le candidat doit choisir deux questions qui donneront lieu à une présentation orale de 5 minutes suivie de 10 minutes de questions et enfin de 5 minutes de présentation de votre parcours d'orientation. Vous devrez nécessairement présenter une question de NSI, éventuellement croisée avec votre autre spécialité. 