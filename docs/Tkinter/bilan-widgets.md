---
title: "Liste des widgets"
author: M.Jarc
tags:
    - Tkinter
---

| Type de widget | Nom tkinter | Liens | Image |
|:--------------:|:-----------:|:-----:|:-----:|
|La fenêtre|`Tk()`|[Bases](Bases/base-fenetre.md) </br> [Configuration](Bonus/bonus-fenetre.md) |![Fenêtre](pics/screen-tk.png)|
| Le bouton | `Button()` | [Bases](Bases/base-bouton.md) </br> [Bouton-image](Bonus/bonus-bouton-image.md) | ![Bouton](pics/screen-button.png)|
| L'étiquette | `Label()` | [Bases](Bases/base-entree-sortie.md)| ![Etiquette](pics/screen-label.png) |
| La barre de saisie | `Entry()` | [Bases](Bases/base-entree-sortie.md)| ![Saisie](pics/screen-entry.png) |
| La frame | `Frame()` | [Bases](Bonus/bonus-conteneurs.md)| ![Frame](pics/screen-frame.png) |
| Le panneau | `PanedWindow()` | [Bases](Bonus/bonus-conteneurs.md)| ![Panneau](pics/screen-paned.gif) |
| Les onglets | `Notebook()` | [Bases](Bonus/bonus-notebook.md)| ![Onglets](pics/screen-notebook.png) |
| Les images | `PhotoImage()` | [Bases](Bonus/bonus-photoimage.md) </br> [Bouton-image](Bonus/bonus-bouton-image.md) </br> [Gestion des images](Bonus/bonus-images.md) </br> [Les canvas](Bonus/bonus-canvas.md)| ![Image](pics/screen-photoimage.png) |
| Le caneva | `Canvas()` | [Bases](Bonus/bonus-canvas.md)| ![Caneva](pics/screen-canvas.png) |
| La barre de menu | `Menu()` et `Menubutton()` | [Bases](Bonus/bonus-menus.md)| ![Menu](pics/screen-menubar.png) |
| L'échelle | `Scale()` | [Bases](Bonus/bonus-scale-spin.md)| ![Echelle](pics/screen-scale.png) |
| La saisie numérique | `Spinbox` | [Bases](Bonus/bonus-scale-spin.md)| ![Saisie numérique](pics/screen-spinbox.png) |
| La liste | `Listbox` | [Bases](Bonus/bonus-list.md)| ![Liste](pics/screen-listbox.png) |
| Le menu déroulant | `Combobox` | [Bases](Bonus/bonus-list.md)| ![Menu déroulant](pics/screen-combobox.png) |
| L'arbre | `Treeview` | [Bases](Bonus/bonus-list.md)| ![Arbre](pics/screen-treeview.png) ![Arbre hiérarchique](pics/screen-treeview2.png) |
| L'éditeur de texte | `Text` | [Bases](Bonus/bonus-text.md)| ![Editeur de texte](pics/screen-text.png) |
| Les ascenseurs | `Scrollbar` | [Bases](Bonus/bonus-scrollbar.md)| ![ascenseurs](pics/screen-scrollbar.png) |
| La barre de progression | `Progressbar` | [Bases](Bonus/bonus-progressbar.md)| ![Barre de progression](pics/screen-progressbar.png) |
