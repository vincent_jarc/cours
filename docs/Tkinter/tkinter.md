---
title: Tkinter
author: M.Jarc
tags:
    - Tkinter
---

# La programmation événementielle avec Tkinter 

<center>![Titre](./pics/title0.png)</center>

Dans ce TP, nous allons programmer des GUI, c’est-à-dire des **Graphic User Interface**, soit **interface graphique**. Le contraire d’une GUI est une CLI (**Command Line Interface** ou **interface en ligne de commande**). 

Pour réaliser nos interfaces graphiques, nous allons utiliser Tkinter, une bibliothèque standard fournie avec Python, basée sur la bibliothèque Tk initialement prévue pour le langage Tcl. D’autres GUI existent et sont bien plus complètes que Tkinter comme GTK, QT, wxWidget… mais il faudra les télécharger. Tkinter a l’avantage d’être natif sous Python. De plus Tkinter propose moins de widgets mais est assez simple d’utilisation et d’installation (logique vu qu’il n’y a pas d’installation du tout). 

- Dans un premier temps, suivez les étapes de la partie **Les bases** dans l'ordre afin de comprendre le fonctionnement de Tkinter. 
- Puis, vous pourrez aborder la partie **Aller plus loin** en piochant comme bon vous semble, au gré de vos besoins. 
- Enfin, retrouvez une [liste des widgets](bilan-widgets.md) proposés par Tkinter. 

Pour plus d'informations sur Tkinter, [consultez la documentation en Français](http://tkinter.fdex.eu/). 
