---
title: "3 - Organiser la fenêtre"
author: M.Jarc
tags:
    - Tkinter
---

# La méthode `pack()`

Pour l'instant, l’empaquetage de vos widgets a été fait de manière expédidtive, mais on peut préciser l'empaquetage grâce à trois paramètres :

- `side` :	 indique si les widgets sont ajoutés vers le haut, le bas, la gauche ou la droite. Les valeurs possibles sont : `TOP`, `BOTTOM`, `LEFT` et `RIGHT`. L’ordre de l’empaquetage est pris en compte, si deux widgets sont empaquetés à gauche, le premier sera tout à gauche, le second un peu plus à droite

- `expand` : indique si le widget devra grandir quand son maître grandira. Les valeurs possibles sont `True` ou `False`. 

- `fill` : indique si le widget doit prendre toute la place disponible. Les valeurs possibles sont `NONE`, `X`, `Y` et `BOTH`. 

!!! example "Exemple"

    ```python
	btn.pack(side = TOP, fill = X, expand = True)`
    ```

# La méthode `grid()`

Cette méthode est assez simple mais ne permet pas d’organiser au mieux votre fenêtre quand le nombre de widgets se multiplie. Une autre méthode consiste à organiser notre fenêtre à la façon d’un tableau comportant plusieurs lignes (*row* en anglais) et plusieurs colonnes (*column* en anglais). Pour cela, on utilisera la méthode `grid()`. Celle-ci prend quelques paramètres : 

- `row` : le numéro de la ligne où sera placé le widget
- `column` : le numéro de la colonne où sera placé le widget
- `rowspan` : (*optionnel*) le nombre de lignes qu’occupera le widget (1 par défaut)
- `columnspan` : (*optionnel*) le nombre de colonne qu’occupera le widget (1 par défaut)
- `sticky` : (*optionnel*) indique si le widget doit "coller" certains bords. Les valeurs possibles correspondent aux points cardinaux : `N`, `S`, `E`, `W`, `EW`, `NS` et `NSEW`. 
- `padx` : (*optionnel*) marge à gauche et à droite du widget (0 par défaut)
- `pady` : (*optionnel*) marge en haut et en bas du widget (0 par défaut)

!!! example "Exemple"

    ```python linenums='1'
    from tkinter import *

    fenetre = Tk()

    #CRÉATION DES WIDGETS
    btn_haut_gauche    = Button(fenetre, text='en haut à gauche')
    btn_bas_gauche     = Button(fenetre, text='en bas à gauche')
    saisie_haut_droite = Button(fenetre, text='en haut à droite')
    saisie_bas_droite  = Button(fenetre, text='en bas à droite')

    #ORGANISATION DES WIDGETS
    btn_haut_gauche.grid(row=1, column=1)
    btn_bas_gauche.grid(row=2, column=1)
    saisie_haut_droite.grid(row=1, column=2)
    saisie_bas_droite.grid(row=2, column=2)

    #BOUCLE PRINCIPALE
    fenetre.mainloop()
    ```