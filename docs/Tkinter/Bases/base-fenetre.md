---
title: "1 - Première fenêtre"
author: M.Jarc
tags:
    - Tkinter
---

![Fenêtre](pics/screen-tk.png)

# Créer une fenêtre

Voici le code minimal pour créer une fenêtre :

!!! example "Exemple"
	```python linenums='1'
	from tkinter import *

	fenetre = Tk()
	#insérer ici le code supplémentaire
	fenetre.mainloop()
	```

Analysons un peu ces quelques lignes : 

- La ligne 1 importe toute la bibliothèque `tkinter` ;
- La ligne 3 crée la fenêtre et l’affiche ; 
- La ligne 5 crée une boucle infinie chargée de traiter les événements : on parle de **boucle événementielle**. Cette ligne doit être écrite à la toute fin de votre code. Et même si vous ne comprenez pas encore son rôle, elle est fondamentale ;
- Tout le code que vous aurez à ajouter devra être situé au niveau de la ligne 4. 

# Paramétrer votre fenêtre

La bibliothèque `tkinter` utilise la notation pointée `fenetre.mainloop()`. On dit que la variable `fenetre` dispose d'une **méthode** `mainloop()`. Mais elle dispose de bien d'autres méthodes, comme par exemple `title()` qui permet de redéfinir le titre de la fenêtre. Voici comment l'utiliser : 

!!! example "Exemple"

	```python
	from tkinter import *

	fenetre = Tk()
	fenetre.title("Ma première fenêtre")
	fenetre.mainloop()
	```

Mais si vous aviez cherché comment faire sur Internet, voici comment on vous aurait présenté cette méthode : `title(self, string=None)`. Le paramètre `self` correspond à l'objet que vous souhaitez paramétrer, c'est celui qui sera écrit au début de la notation pointée. Donc finalement, vous n'écrirez qu'un seul paramètre quand vous utiliserez cetet méthode, le paramètre `string` qui correspond au texte que vous souhaitez afficher. 

!!! warning "Remarque"

	La méthode `title()` renvoie également un résultat : le titre actuel de la fenêtre. Si vous souhaitez le connaître, il vous suffit donc d'écrire `titre = fenetre.title()`. 

# Quelques méthodes supplémentaires

- `destroy(self)` : Détruit la fenêtre et tous ses descendants. Le paramètre `self` correspond à votre fenêtre. 
- `configure(self, cnf = None, **kw)` ou `config(self, cnf = None, **kw)` : Configure la fenêtre
	- `self` : la fenêtre
	- `cnf` : oubliez-le
	- `**kw` : les mots clés (keywords), dont voici une liste non exhaustive : 
		- `background` ou `bg` : couleur de fond (exemple : `"black"` ou `"#ffffff"`)
		- `width`, `height`: largeur et hauteur
		- `cursor` : image utilisée pour la souris (exemple : `"crosshair"`)
		- `relief` : relief de la fenêtre lorsqu’elle a une bordure (valeurs : `FLAT, SUNKEN, RAISED, GROOVE, RIDGE`)
		- `borderwidth` : largeur des bords de la fenêtre

!!! example "Exemple"

	```python
	if choix == 0:
		fenetre.destroy()
	else: 
		fenetre.configure(
			borderwidth = 4, 
			width = 250, 
			relief = GROOVE)
	```

!!! warning "Remarque"

	Les mots clés utilisés par les méthodes `config()` peuvent être utilisés d'une autre manière. Ainsi, au lieu d'écrire `fenetre.configure(borderwidth = 4)` on peut écrire `fenetre['borderwidth'] = 4`. Inversement, on peut connaître la valeur de ce mot clé en écrivant `print(fenetre['borderwidth'])`. 