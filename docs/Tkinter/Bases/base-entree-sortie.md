---
title: "4 - Faire des entrées-sorties"
author: M.Jarc
tags:
    - Tkinter
---

![Etiquette](pics/screen-label.png)
![Saisie](pics/screen-entry.png)

# Barre de saisie et étiquette

Nous allons avoir besoin des barres de saisie de texte pour permettre à l’utilisateur de taper du texte et ensuite le récupérer pour un éventuel traitement. Bref, un widget permettant de gérer les entrées à la façon de la fonction `input()`. En Tkinter, ces barres de saisie se nomment des `Entry` et elles n'ont besoin que du paramètre `master`. 

!!! example "Exemple"

	```python
	ma_saisie = Entry(master = fenetre)
	```

A l'inverse, nous avons besoin d'un widget gérant les sorties afin de remplacer nos sempiternels `print()`. Ce widget c'est la zone de texte non modifiable aussi appelée **étiquette**. Pour Tkinter ce sera un `Label`. A la construction, il faut transmettre des paramètres `master` et `text` (qui sera le texte par défaut). 

!!! example "Exemple"

	```python
	mon_label = Label(master = fenetre, text = "Texte par défaut")
	```

# Récupérer le contenu de la barre de saisie

On appelle *callback* ou *handler* une fonction qui se déclenche lorsqu’un événement se produit. Dans notre cas, le handler sera exécuté lorsque l'utilisateur cliquera sur un bouton, nous l'appellerons donc `on_mon_bouton_click()`. Définissez-le au début de votre programme. Pour fonctionner, notre callback a besoin qu’on lui transmette l'`Entry` concernée. Les Entry disposent d’une méthode `get()` permettant de récupérer leur contenu sous la forme d'une chaîne de caractère de type `str`. 

!!! example "Exemple"
	```python
	def on_mon_bouton_click(barre_saisie : Entry):
		txt = barre_saisie.get()
		print(txt)
	```

# Modifier le Label

Vous remarquerez que votre fonction ne renvoit pas de résultat, elle se contentera d’un affichage dans l’interpréteur. Pour modifier le contenu de votre `Label`, votre fonction `on_mon_bouton_click()` devra faire appel à la méthode `configure()` ou `config()` de votre `Label`. Par conséquent, votre fonction devra également avoir un second paramètre : 

!!! example "Exemple"

	```python
	def on_mon_bouton_click(
			barre_saisie : Entry, 
			une_etiquette : Label):
		txt = barre_saisie.get()
		une_etiquette.config(text = txt)
	```

!!! warning "Remarque"

	Les `Label` peuvent également contenir des images (même si ce n'est pas vraiment leur rôle premier). Consultez la section [Mettre une image dans un bouton](bonus-bouton-image.md) pour en savoir plus.

# Activer le handler

Vous aurez ensuite besoin de créer un bouton avec un paramètre `command`. Le problème, c'est qu'il faudrait écrire `command = on_mon_bouton_click(ma_saisie, mon_label)` mais il ne faut pas écrire de parenthèses ! Et écrire `command = on_mon_bouton_click` empêche de transmettre la bonne `Entry`. La solution consiste à utiliser les fonctions anonymes de Python : 

!!! example "Exemple"

	```python
	mon_bouton = Button(
		master = fenetre, 
		text = "Dire bonjour", 
		command = lambda: bonjour(ma_saisie, mon_label)
		)
	```