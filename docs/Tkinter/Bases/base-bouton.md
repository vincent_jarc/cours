---
title: "2 - Premier bouton"
author: M.Jarc
tags:
    - Tkinter
---

![Bouton](pics/screen-button.png)

# Principe général des widgets

Reste maintenant à ajouter des éléments graphiques à votre fenêtre, c'est ce qu'on appelle des **widgets**. Pour créer n’importe quel widget avec tkinter il vous suffira de connaître son nom. Tous sont créés par une fonction du type : 

!!! example "Exemple"

    `variable = NomWidget(master, **kw)`

Vous noterez la majuscule systématique au début du nom du widget. Le paramètre `master` indique le widget parent, celui dont il dépend. En général ce sera votre fenêtre principale. Le paramètre `**kw` indique les éventuels mot-clés (keywords) permettant de le paramétrer. 

# Créer

Pour notre premier widget nous allons créer un bouton. En tkinter, un bouton se nomme... `Button` (avec deux T) et le premier mot clé à retenir est `text` qui permettra de définir le texte à afficher sur le bouton. 

!!! example "Exemple"

    ```python
    btn_fermer = Button(master = fenetre, text = 'Quitter')
    ```

# Dessiner

Une fois votre bouton créé, il ne se passe rien : il faut encore l’empaqueter, c’est-à-dire le dessiner dans votre fenêtre. Pour cela, il y a plusieurs méthodes, mais la plus expéditive est la suivante : 

!!! example "Exemple"

    ```	python
    btn_fermer.pack()
    ```

Le bouton est automatiquement ajouté à son widget maître (ici la fenêtre). 

# Attribuer une action

Pour l'instant, votre bouton s'affiche bien mais ne fait rien. Il faut lui associer une fonction à exécuter, c'est ce qu'on appelle un **callback**. Cela peut se faire dès la création du bouton où avec la méthode `config()` : 

!!! example "Exemple"

    ```python
    btn_fermer = Button(master = fenetre, text = 'Quitter', command = fenetre.destroy)
    btn_fermer.pack()
    ```
    ```python
    btn_fermer = Button(master = fenetre, text = 'Quitter')
    btn_fermer.config(command = fenetre.destroy)
    btn_fermer.pack()
    ```
    ```python
    btn_fermer = Button(master = fenetre, text = 'Quitter')
    btn_fermer["command"] = fenetre.destroy
    btn_fermer.pack()
    ```

!!! warning "Remarque"

    Vous noterez que l'on a ôté les parenthèse à `fenetre.destroy()`. Et ce n'est pas une erreur ! 

# Configurer

D’autres mots clés (parfois déjà vus pour les fenêtres) peuvent être utilisés comme `background, foreground, image, relief, borderwidth, cursor, font, width, height`… Ces mots-clés peuvent être utilisés lors de la création du bouton ou séparément avec `configure()`. 

!!! example "Exemple"

    ```python
    btn.configure(font='Arial 12') 
    btn.config(font=('Comic sans MS',16))
    ```

<center>Pour en connaître plus [cliquez ici](http://tkinter.fdex.eu/doc/bw.html).</center>