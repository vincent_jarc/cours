---
title: Les boîtes de dialogue
author: M.Jarc
tags:
    - Tkinter
---

# Utilisation basique

Pour utiliser les boîtes de message, vous devez commencer par importer une sous-bibliothèque : `from tkinter.filedialog import *`. Ce module contient des fonctions comme : 

- `askdirectory()` : ouvre une boîte de dialogue pour sélectionner un dossier, 
- `askopenfilename()` : ouvre une boîte de dialogue pour sélectionner un fichier, 
- `askopenfilenames()` : ouvre une boîte de dialogue pour sélectionner un ou plusieurs fichiers, 
- `asksaveasfilename()`  : ouvre une boîte de dialogue pour enregistrer un fichier, 

Ces fonctions prennent un paramètre `parent` (votre fenêtre principale) mais il est aussi possible de définir des paramètres comme `title` pour le titre de la boîte de dialogue ou `initialdir` pour le répertoire ouvert au départ. Ces fonctions renvoient comme résultat une chaîne de caractère detyp `str` correspondant à l’adresse absolue du fichier ou dossier (`askopenfilenames()` renvoie plus exactement une `list` de `str`, éventuellement vide). 

!!! example "Exemple"

	On ouvre un fichier et on affiche son contenu

	```python linenums='1'
	filename = askopenfilename(fenetre, title = "Charger une partie", initialdir = "./saves")
	if filename != "":
		fichier = open(filename, mode = "r")
		print(fichier.read())
		fichier.close()
	```

# Paramétrer l'affichage

Il est également possible de restreindre l'affichage des fichiers pour l'utilisateur, par exemple en n'affichant que les fichiers portant une extension spécifique. Pour cela, il existe un paramètre `filetypes` prenant en valeur un tableau de tuples : 

!!! example "Exemple"

	```python linenums='1'
	filename = askopenfilename(
		fenetre, 
		title = "Charger une partie", 
		initialdir = "./saves"
		filetypes = [
			("Fichiers texte", "*.txt"), 
			("Fichiers JSON", "*.bonus"), 
			("Tous les fichiers", "*.*")]) 
	#L'utilisateur peut choisir de ne voir que les .txt, que les .bonus ou alors tous les fichiers
	```
