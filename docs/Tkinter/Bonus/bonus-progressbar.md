---
title: La barre de progression
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

![Barre de progression](pics/screen-progressbar.png)

# Création

La barre de progression, appelée `Progressbar` n'est pas un widget disponible nativement avec la bibliothèque Tkinter, il faut faire appel au sous-module `tkinter.ttk`. Ses principaux paramètres sont : 

- `master` : le widget maître, généralement la fenêtre
- `orient` : indique l'orientation de la barre (`HORIZONTAL` ou `VERTICAL`)
- `length` : indique la longueur de la barre en pixels
- `mode` : peut prendre les valeurs `"determinate"` (par défaut) et `"indeterminate"`. Dans le premier cas, la barre de progression a un minimum et un maximum, dans le second la barre oscille de gauche à droite (ou de haut en bas) en attendant la fin d'un processus
- `value` : (`int`) la valeur actuelle de la barre de progression
- `maximum` : (`int`) la valeur maximale de la barre de progression

!!! example "Exemple"

	```python
	barre_de_progression = Progressbar(
		master = fenetre, 
		orient = HORIZONTAL, 
		length = 200, 
		maximum = 50)
	```

# Modification

Une façon simple de modifier votre barre de progression consiste à actualiser le paramètre `value` : 

!!! example "Exemple"

	```python
	barre_de_progression.config(value = 10)
	barre_de_progression['value'] += 5
	```

Mais de nombreuses méthodes sont proposées, à commencer par la fonction `step()` qui incrémente automatiquement la barre de progression. Le montant de ce *pas* peut être paramétrer en indiquant par exemple `barre_de_progression.step(amount = 5)`. 

# Modification automatique

Si vous souhaitez une barre de progression servant simplement à mettre l'utilisateur en attente (une barre en mode `"indeterminate"`), vous serez sûrement content de savoir qu'il est possible de demander à tkinter d'incrémenter tout seul cette barre. Pour cela, vous n'avez qu'à utiliser la méthode `start()` en lui indiquant un paramètre `interval` en millisecondes et le tour est joué. Pour y mettre fin, une méthode `stop()` existe également. 

!!! example "Exemple"

	```python
	barre_de_progression.start(interval = 500) #on augmente toutes les 0,5s
	barre_de_progression.stop()                #on arrête
	```