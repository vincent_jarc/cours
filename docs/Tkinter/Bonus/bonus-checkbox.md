---
title: Les boutons à cocher et les boutons radio
author: M.Jarc
tags:
    - Tkinter
---

# Les variables Tkinter

Pour savoir si une case à cocher est cochée ou non, vous aurez besoin d'utiliser des variables spécifiques à Tkinter : les `IntVar`. Elles ont les mêmes valeurs que les `int` mais ont l'avantage d'évoluer en même temps que l'interface. En revanche, leur création se fait à la manière des widgets, ce qui peut paraître un peu lourd. 

!!! example "Exemple"

	```python
	fenetre = Tk()
	n = IntVar(master = f, value = 5)
	fenetre.mainloop()
	```

De même, il n'est pas possible de les modifier en écrivant `n = 7` ou de les lire en écrivant `print(n)`. Vous devez passer par des fonctions spécifiques appelées `set()` et `get()` : 

!!! example "Exemple"

	```python
	n.set(7) 			#équivalent à n = 7
	print(n.get())  	#équivalent à print(n)
	n.set(n.get() + 1)  #équivalent à n = n + 1
	```

!!! warning "Remarque"

	Python propose également des `DoubleVar` pour remplacer les `float` et des `StringVar` pour remplacer les `str`. Elles fonctionnent sur le même modèle que les `IntVar`.

# Créer une case à cocher

En tkinter, les cases à cocher s'appellent des `Checkbutton`. Comme les boutons normaux, elles prennent des attributs `text`, `image` ou `command`. Mais l'attribut principal est `variable` qui permet de lier la case à cocher à une variable spécifique. Par défaut, cette variable vaudra 0 quand la case sera blanche et 1 quand elle sera cochée. 

!!! example "Exemple"

	```python
	case_a_cocher = Checkbutton(
		master = fenetre, 
		text = "Je veux des frites !", 
		variable = n)
	```

Si pour une raison ou une autre vous avez besoin de cocher ou décocher automatiquement votre `Checkbutton`, il existe deux fonctions `select()` et `deselect()` ainsi qu'une fonction `toggle()` qui change l'état de la case. 

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/chw.html). 

# Créer des boutons radios

En tkinter, les cases à cocher s'appellent des `Radiobutton`. Elles fonctionnent de manière assez similaire aux cases à cocher. Pour que plusieurs boutons radios fonctionnent ensemble (donc qu'en cliquant sur l'un, cela désélectionne les autres), il suffit qu'ils partagent une même variable. Il faudra toutefois spécifier la valeur associée à chaque bouton avec l'attribut `value`. 

!!! example "Exemple"

	```python
	n = IntVar(fenetre, value = 1)
	bouton_radio1 = Radiobutton(
		master = fenetre, text = "Frites !", 
		variable = n, value = 1)
	bouton_radio2 = Radiobutton(
		master = fenetre, text = "Haricots !", 
		variable = n, value = 2)
	bouton_radio3 = Radiobutton(
		master = fenetre, text = "Ratatouille !", 
		variable = n, value = 3)
	```

Vous pouvez également utiliser une variable `StringVar` plutôt que `IntVar` : 

!!! example "Exemple"

	```python
	n = StringVar(fenetre, value = "Frites")
	bouton_radio1 = Radiobutton(
		master = fenetre, text = "Frites !", 
		variable = n, value = "Frites")
	bouton_radio2 = Radiobutton(
		master = fenetre, text = "Haricots !", 
		variable = n, value = "Haricots")
	bouton_radio3 = Radiobutton(
		master = fenetre, text = "Ratatouille !", 
		variable = n, value = "Ratatouille")
	```

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/radbw.html). 
