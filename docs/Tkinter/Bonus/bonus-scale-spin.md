---
title: Les échelles et les saisies numériques
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

# Les échelles

![Echelle](pics/screen-scale.png)

Les échelles, ou intervalles de sélection, sont appelées `Scale` en Tkinter. Leurs principaux attributs sont : 

- `master` : le widget maître, comme d'habitude
- `orient` : l'orientation de l'échelle (`HORIZONTAL` ou `VERTICAL`)
- `length` : (`int`) la longueur en pixels de l'échelle
- `from_`, `to` : (`int` ou `float`) ces attributs indiquent les valeurs minimales et maximales de l'échelle
- `resolution` : (`int` ou `float`) cet attribut indique le pas, c'est-à-dire la valeur minimal de déplacement du curseur

!!! example "Exemple"

	```python
	mon_echelle = Scale(
		master = fenetre, 
		orient = HORIZONTAL,
		length = 200, 
		from_ = 0,
		to = 10,
		resolution = 0.1)
	```

Mais d'autres attributs sont disponibles. Sans rentrer dans les détails, voici une liste des plus utiles : 

- `tickinterval` : (`int` ou `float`) lorsque sa valeur est non nulle, cela permet d'indiquer des valeurs intermédiaires sur l'échelle ainsi que leur espacement
- `label` : (`str`) ajoute une légende à votre échelle
- `showvalue` : à mettre à 0 si vous ne souhaitez pas que la valeur actuelle soit affichée

Pour lire ou écrire la valeur du curseur, vous avez deux options : 

- soit attribuer une variable de type `IntVar` à votre échelle lors de sa création 
- soit utiliser des méthodes `get()` et `set()`

!!! example "Exemple"

	```python
	la_valeur = IntVar()
	mon_echelle.config(variable = la_valeur)
	```

	```python
	print(mon_echelle.get()) #on affiche la valeur actuelle
	mon_echelle.set(17)		 #on modifie sa valeur
	```

Si ces deux possibilités vous semblent obscures, allez voir la section [Les boutons à cocher et les boutons radio](bonus-checkbox.md) pour comprendre le principe. 

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/scw.html). 

# Les saisies numériques

![Echelle](pics/screen-spinbox.png)

Les saisies numériques sont appelées `Spinbox` en Tkinter. Elles utilisent des paramètres similaires au `Scale`, vous retrouverez ainsi les attributs `from_`, `to` et `increment` (en remplacement de `resolution`). Vous retrouverez également une méthode `get()` mais pas `set()`. Attention, `get()` ne renverra pas un nombre mais une chaîne de caractères. 

En effet, il est possible d'utiliser les `Spinbox` pour manipuler du texte. Dans ce cas, au lieu d'utiliser les attributs `from_` et `to`, vous utiliserez l'attributs `values` en lui fournissant un tuple de chaînes de caractères (en écrivant par exemple `values = ('bleu', 'blanc', 'rouge')`). Vous pourrez ensuite récupérer la valeur choisie à l'aide de la méthode `selection_get()`. 

Dernière remarque, si vous utiliser votre `Spinbox` pour saisir une valeur numérique flottante, vous aurez certainement besoin de préciser le format des nombres. Pour cela utilisez l'attribut `format` suivi d'une chaîne de caractère. 

!!! example "Exemple"

	Pour afficher un nombre à trois chiffres dont un après la virgule :

	```python
	ma_saisie = Spinbox(
		master = fenetre,
		from_ = 0, 
		to = 100,
		format = "%3.1f"
		)
	```

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/spbw.html). 