---
title: Les événements Tkinter
author: M.Jarc
tags:
    - Tkinter
---

# Une courte liste d'événements

Lorsque vous bougez la souris au-dessus ou en dehors d’un widget, lorsque vous appuyez sur une touche du clavier ou un bouton de la souris… vous déclenchez un événement (*event* en anglais). Quand votre souris reste immobile au-dessus d’un widget, elle continue d’émettre des événements ! C’est un peu comme si vous étiez un voleur au beau milieu d’un musée hyper sécurisé en pleine nuit : tout mouvement, toute action et même votre simple présence sont des signaux pour les caméras de sécurité ou les détecteurs de mouvement. Voici une liste d’événements parmi les plus utiles : 

- `'<Button-1>'` : l’utilisateur a cliqué sur le widget avec le bouton gauche de la souris
- `'<Double-Button-3>'` : l’utilisateur a double-cliqué sur le widget avec le bouton droit de la souris
- `'<ButtonRelease-2>'` : l’utilisateur a relâché la molette de la souris
- `'<MouseWheel>'` : l’utilisateur a tourné la molette de la souris
- `'<Enter>'` : l’utilisateur a bougé la souris au-dessus du widget
- `'<Leave>'` : l’utilisateur a bougé la souris en-dehors du widget
- `'<Return>'` : l’utilisateur a appuyé sur la touche Entrée
- `'<Escape>'` : l’utilisateur a appuyé sur la touche Echap
- `'<Space>'` : l’utilisateur a appuyé sur la touche Espace
- `'<Return>'` : l’utilisateur a appuyé sur la touche Entrée
- `'z'` : l’utilisateur a appuyé sur la touche z (vous trouverez ici une [liste des touches du clavier](https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.html))
- `'<Key>'` : l’utilisateur a appuyé sur une touche quelconque du clavier

# Lier un widget à un handler

Si l’on reprend la métaphore du voleur dans le musée alors celui qui s’occupe des caméras et détecteurs de mouvements est le **gestionnaire d’événements**. Et vous l’avez toujours écrit puisqu’il s’agit du `mainloop()`. Cette boucle infinie a pour rôle de surveiller l’activité de votre fenêtre et de capter tous les signaux émis par les widgets dus au moindre événement. Une fois l’événement capté, `mainloop()` se charge d’y associer le handler correspondant.

Mais pour cela, il faut avoir préalablement lié entre eux un handler, un widget et un événement. Pour les boutons, cela se fait lors de la création ou de la configuration avec le mot-clé `command` et pour le clic gauche seulement. Mais Pour d’autres widgets ou d’autres événements, il faut faire cette liaison soi-même avec la méthode `bind()` : 

!!! example "Exemple"

	```python
	un_bouton.bind('<Button-2>', un_handler)
	```

Une méthode `bind_all()` est également disponible, elle lie toute l’application à cet événement, utile par exemple pour les touches F1, ESCAPE… pour obtenir de l’aide ou quitter. Mais à utiliser avec parcimonie. 

# Revoir nos handlers

Si vous avez testé ce qui précède, vous avez du vous rendre compte qu’un problème se pose quant au nombre de paramètres. Avec la méthode `bind()`, les handlers reçoivent automatiquement un paramètre supplémentaire : l’événement déclencheur ! 

Deux solutions s'offrent à vous : 
- redéfinir votre handler pour qu'il prenne un paramètre de type `Event` ; 
- travailler avec une fonction anonyme ayant un paramètre de type `Event`, quitte à ne pas l'utiliser.  

!!! example "Exemple"

	*Méthode 1*

	```python
	def un_handler(evt : Event): 
		...
	```

	*Méthode 2*

	```python
	fenetre.bind('<Escape>', lambda evt: fenetre.destroy())
	```

# Manipuler les événements

Les événements de type `Event` embarquent avec eux diverses informations. Trois informations pourraient vous être utiles, `keysym`, `x` et `y` : 

!!! example "Exemple"
	```python
	def un_handler(evt : Event): 
		print(evt.keysym)   #affiche le nom de la touche du clavier
		print(evt.x, evt.y) #affiche la position du clic de souris
	```
