---
title: Les PhotoImage
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

![Image](pics/screen-photoimage.png)

# Créer une PhotoImage

Si vous souhaitez afficher une image dans un `Button`, un `Label` ou plus généralement dans un `Canvas`, vous devrez préalablement créer une `PhotoImage`. Celle-ci ne prend pas de `master`, ce n'est pas un widget mais un élément chargé en mémoire qui servira pour d'autres widgets. 

!!! example "Exemple"

    ```python
    img_quelconque = PhotoImage(file = 'nomDuFichier.png')
    ```

Enfin, si vous souhaitez connaître la largeur et la hauteur de vos `PhotoImage`, celles-ci disposent des méthodes `width(self)` et `height(self)`. 

!!! example "Exemple"

    ```python
    largeur = img_quelconque.width()
    hauteur = img_quelconque.height()
    ```

# Quelques précautions

Les `PhotoImage` peuvent vite s'avérer lourdes, il est donc préférable de ne les charger qu'une seule fois, même si elles doivent servir plusieurs fois. Voici quelques pratiques utiles : 

- Utiliser des **images globales** : étant donné que vous ne modifierez certainement pas ces `PhotoImage`, il ne semble pas déraisonnable d'en faire des constantes globales, stockées dans une même bibliothèque

!!! example "Exemple"

    ```python
    IMG_FORET = PhotoImage(file = 'images/case_de_foret.png')
    IMG_MONTAGNE = PhotoImage(file = 'images/case_de_montagne.png')
    ```

- Utiliser des **conteneurs** : il se peut que vous ayez de nombreuses images et plutôt que de créer une variable par image, il peut être préférable de les stocker dans un conteneur adapté : un tableau de type `list` ou un dictionnaire de type `dict`. Mieux, vous pouvez ensuite automatiser leur chargement. 

!!! example "Exemple"

    *Avec un tableau :*

    ```python
    IMAGES = []
    IMAGES.append(PhotoImage(file = 'images/case_de_foret.png'))    #La forêt : IMAGES[0]
    IMAGES.append(PhotoImage(file = 'images/case_de_montagne.png')) #La montagne : IMAGES[1]
    ```
    *Avec un dictionnaire :*

    ```python
    IMAGES = {}
    IMAGES["forêt"] = PhotoImage(file = 'images/case_de_foret.png')       #La forêt : IMAGES['forêt']
    IMAGES["montagne"] = PhotoImage(file = 'images/case_de_montagne.png') #La montagne : IMAGES['montagne']
    ```
