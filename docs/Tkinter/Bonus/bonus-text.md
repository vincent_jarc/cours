---
title: Les éditeurs de texte
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

![Editeur de texte](pics/screen-text.png)

Tkinter propose un widget de type `Text` permettant d'aller bien plus loin que les `Label` et les `Entry`. Ce widget permet de créer un véritable éditeur de texte, à la façon d'un Notepad++, d'un TextEdit ou d'un Wordpad. Le texte peut être écrit sur plusieurs lignes, contenir des mises en formes diverses… 

# Créer un éditeur de texte

Sa création se fait facilement, si le widget dispose de nombreux attributs, aucun n'est réellement essentiel à sa mise en place. Voici toutefois quelques paramètres utiles :

- `bg` ou `background` : indique la couleur de fond (`"white"` par défaut)
- `fg` ou `foreground` : indique la couleur du texte (`"black"` par défaut)
- `bd` ou `borderwidth` : indique la largeur en pixels du bord du widget
- `font` : indique la police à utiliser par défaut (*exemple : `"Tahoma 12 italic"`*)
- `width`, `height` : indiquent la largeur et la hauteur de la zone de texte exprimée en caractères et non en pixels (`width = 5, height=20` affichera 5 lignes de 20 caractères maximum)
- `wrap` : indique le comportement à suivre quand une ligne est trop longue. La valeur par défaut, `wrap = "char"`, effectue un retour à la ligne brutal (même en milieu de mot) alors que `wrap = "word"` ne coupe pas les mots. Une valeur `wrap = "none"` existe signifiant qu'il n'y aura aucun retour à la ligne si celle-ci est trop longue, mais c'est assez peu utile dans notre cas et nécessitera une barre de défilement horizontale
- `selectbackground`, `selectforeground`, `selectborderwidth` : indiquent la couleur d'arrière plan, la couleur du texte et la largeur de la bordure quand un texte est sélectionné. 

!!! example "Exemple"

	```python
	editeur_texte = Text(fenetre, wrap = "word", selectbackground = "#ff0000") #sélection en rouge
	```

# Indices et lecture du contenu

Pour récupérer le contenu de l'éditeur de texte, il existe une méthode `get()` un peu particulière. Celle-ci prend deux paramètres `index1` et `index2` indiquant le début et la fin du texte à saisir. Ces indices sont des chaînes de caractères dont la forme générale est `numeroDeLigne.numeroDeColonne`. Par exemple, le tout début du texte porte l'indice `"1.0"` pour ligne 1 et colonne 0. D'autres valeurs sont possibles : 

- `numeroDeLigne.end` pour l'indice situé à la fin d'une ligne. 
- `end` pour l'indice de la fin du texte
- `insert` pour l'indice du curseur
- `sel.first` et `sel.last` pour les indices de début et de fin de texte sélectionné. Si aucun texte n'est sélectionné, une erreur sera levée. Cela peut se régler à l'aide d'un bloc `try... except`.

!!! example "Exemple"

	`tout_afficher()` affiche le texte complet, `afficher_ligne()` affiche la ligne où le curseur est situé et `afficher_selection()` affiche la zone sélectionnée.

	```python
	def tout_afficher(un_editeur):
		print(un_editeur.get("1.0", "end"))

	def afficher_ligne(un_editeur):
		print(un_editeur.get("insert linestart", "insert lineend"))

	def afficher_selection(un_editeur):
		try:
			print(un_editeur.get("sel.first", "sel.last"))
		except:
			print("Aucun texte sélectionné")
		
	```

# Les étiquettes

Si vous souhaitez mettre en forme votre texte, vous allez devoir faire appel aux **étiquettes** ou **tags**. Pour commencer, vous devrez configurer votre tag avec `tag_config()` en lui attribuant un nom pour l'identifier ainsi que des attributs de mise en forme : on retrouve les habituels `font`, `background`, `foreground`... mais également des attributs `underline` et `overstrike` pour souligner ou rayer (en les mettant à 1), un attribut `justify` pour aligner le texte (valeurs : `left`, `right` ou `center`). 

Une fois vos tags créés et configurés, il ne vous reste plus qu'à les utiliser avec `tag_add()` ou à les supprimer avec `tag_remove()`. Ceux-ci prennent trois paramètres : 

- `tagName` : le nom que vous avez choisi pour votre tag, 
- `index1` : l'indice du début
- `index2` : l'indice de fin. Si cet indice n'est pas renseigné, 

!!! example "Exemple"

	```python
	editeur_texte.tag_config("surligneJaune", background = "yellow")

	def surligner(un_editeur):
		try:
			un_editeur.tag_add("surligneJaune", "sel.first", "sel.last")
		except:
			print("Sélectionnez du texte !")

	def desurligner(un_editeur):
		try:
			un_editeur.tag_remove("surligneJaune", "sel.first", "sel.last")
		except:
			print("Sélectionnez du texte !")
	```

# L'annulation

Avec un widget `Text`, vous pouvez même mettre en place un système Annuler / Rétablir les modifications. Pour cela, vous devez mettre l'attribut `undo` à `True` lorsque vous créez votre widget. Un attribut `maxundo` vous permet également de limiter le nombre d'annulations possibles. Ensuite, le widget dispose de deux méthodes `edit_undo()` pour annuler et `edit_redo()` pour rétablir. 

!!! example "Exemple"

	```python
	editeur_texte.config(undo = True, maxundo = 5)

	def annuler(un_editeur):
		try:
			un_editeur.edit_undo()
		except:
			print("Plus de retour arrière possible.")

	def retablir(un_editeur):
		try:
			un_editeur.edit_redo()
		except:
			print("Plus de retour avant possible.")
	```

Pour en savoir plus, [consultez la documentation en Français](http://tkinter.fdex.eu/doc/textw.html).