---
title: Les Canvas
author: M.Jarc
tags:
    - Tkinter
	- Canvas
---

![Image](pics/screen-photoimage.png)
![Caneva](pics/screen-canvas.png)

# Le conteneur Canvas

Le conteneur Canvas permet de dessiner des figures, des images ou de placer des widgets au pixel près. Vous retrouverez les mot-clés habituels, mais n’oubliez pas de préciser la largeur et la hauteur de votre Canvas. 

!!! example "Exemple"

	```python
	mon_caneva = Canvas(
		master = fenetre, 
		bg = 'white', 
		width = 600, 
		height = 400)
	```

# Dessiner dans un Canvas

Voici ensuite quelques méthodes de dessin élémentaires : 

- `create_oval(self, x0,y0,x1,y1,**kw)` et `create_rectangle(self, x0,y0,x1,y1,**kw)`: dessinent une ellipse et un rectangle :
	- `x0,y0` : coordonnées en haut à gauche de la figure
	- `x1,y1` : coordonnées en bas à droite de la figure
	- `**kw` : mots clés pour paramétrer la figure :
		- `fill` pour la couleur de remplissage,
		- `outline` pour la couleur du bord, 
		- `dash` pour mettre en pointillés (prend comme valeur une liste de nombres entiers)
- `create_text(self, x, y, text, **kw)` : écrit du texte dans le Canvas. 
	- `x, y` : coordonnées du texte dans le Canvas
	- `text` : le texte à afficher
	- `**kw` : mots clés pour paramétrer la figure :
		- `fill` pour la couleur du texte,
		- `font` pour la police d'écriture (par exemple : `"Times 16 italic bold"`)
- `place(self, widget, x, y)` : place un widget fils dans le Canvas. 
	- `widget` : un widget quelconque
	- `x, y` : coordonnées du widget dans le Canvas

!!! example "Exemple"

	```python linenums='1'
	from tkinter import *

	fenetre = Tk()
	mon_caneva = Canvas(
		master = fenetre, 
		width = 400, height = 300, 
		bg = 'black')
	mon_caneva.pack()
	mon_rectangle = mon_caneva.create_rectangle(
		50, 50, 150, 200,
		width = 5.0,
		fill = 'red', 
		outline = '#0000FF',
		dash = [2,1])
	mon_bouton = Button(master = mon_caneva, text = 'OK')
	mon_bouton.place(x = 200, y = 250)
	fenetre.mainloop()
	```

# Ajouter des images à un Canvas

Pour dessiner une image vous devez d’abord créer un widget de type `PhotoImage` (voir la section sur les [PhotoImage](bonus-photoimage.md)) puis le dessiner dans votre canvas grâce à la méthode `create_image()` : 

!!! example "Exemple"

	```python
	mon_image = PhotoImage(file = 'une_image.png')
	mon_dessin = mon_caneva.create_image(
		15, 35, 
		image = mon_image, 
		anchor = NW)
	```

Votre programme doit garder une trace de ces images en les enregistrant dans une variable ou une liste. Pour l’explication technique, sachez que `create_image()` renvoie un identifiant, si cet identifiant est perdu, l'espace mémoire qui lui est attribué sera automatiquement supprimé par le ramasse-miette de Python, un logiciel chargé d’éviter le gaspillage de mémoire.

L’attribut `anchor` permet de connaître le point faisant référence pour le placement de l’image (centre, nord-ouest, sud-est…). Ici, cela signifie que c'est le coin supérieure gauche de l'image qui sera placé aux coordonnées `(15, 35)`

# Modifier un dessin existant

Conserver l'identifiant de vos constructions a un autre avantage : permettre de les retrouver et de les modifier. Citons deux méthodes : 

- `move(self, tagOrId, dx, dy)` : permet de déplacer un item du `Canvas`. 
	- `self` : votre `Canvas`, 
	- `tagOrId` : l'identifiant de votre figure enregistré dans une variable, 
	- `dx, dy` : déplacement horizontal et vertical
- `coord(self, tagOrId, x0, y0, x1, y1...)` : redéfinit les coordonnées d'un item du `Canvas`
	- `self` : votre `Canvas`, 
	- `tagOrId` : l'identifiant de votre figure enregistré dans une variable, 
	- `x0, y0, x1, y1...` : les nouvelles coordonnées de l'item
- `delete(self, tagOrId)` : supprimer l'item portant l'identifiant `tagOrId`

!!! example "Exemple"

	```python
	mon_caneva.move(mon_dessin, 100, 0) #déplace l'image de 100px vers la droite
	mon_caneva.coords(mon_rectangle, 50, 50, 300, 400) #redimensionne le rectangle
	mon_caneva.delete(mon_dessin) #supprimer l'image
	```

Enfin, sachez qu'il est possible de marquer vos figures, de leur aposer un ou plusieurs *tags* lors de leur création. Ces tags permettent ensuite de leur appliquer des modifications, éventuellement par groupe. 

!!! example "Exemple"

	Ici on dessine deux montagnes sur le Canvas puis on les décale toutes les deux vers le bas en une seule instruction.

	```python
	image_montagne = PhotoImage(file = 'montagne.png')
	dessin1 = mon_caneva.create_image(0, 0, image = image_montagne, tag = "montagne")
	dessin2 = mon_caneva.create_image(50, 80, image = image_montagne, tag = "montagne")
	mon_caneva.move("montagne", 0, 50)
	```