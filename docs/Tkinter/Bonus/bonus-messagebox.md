---
title: Les boîtes de message
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

Pour utiliser les boîtes de message, vous devez commencer par importer une sous-bibliothèque : `from tkinter.messagebox import *`. Ce module contient des fonctions comme : 

- `showinfo(), showerror(), showwarning()` : permettent d’ouvrir des boîtes de message d’information, d’erreur ou d’avertissement. Elles ne renvoient aucun résulat mais prennent deux paramètres : 
	- `title` : le titre de la boîte de dialogue, 
	- `message` : le texte du message ; 
- `askokcancel(), askretrycancel(), askyesno(), askyesnocancel()` : permettent d’ouvrir des boîtes de message avec deux boutons oui/non, ok/annuler… Elles renvoient comme résultat `True`, `False` ou `None` et prennent elles aussi les deux mêmes paramètres. 

!!! example "Exemple"

	```python
	quitter = askyesno(title = "Répondez !", message = "Voulez-vous quitter ?")
	if quitter :
		fenetre.destroy()
	else : 
		print("Show must go on !")
	```