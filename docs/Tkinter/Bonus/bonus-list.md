---
title: Les listes, les menus déroulants et les arbres
author: M.Jarc
tags:
    - Tkinter
---

# Les listes

![Liste](pics/screen-listbox.png)

Les listes proposent à l'utilisateur de sélectionner un (ou plusieurs) textes parmi une liste. Elles sont appelées `Listbox` en tkinter et prennent les attributs suivants : 

- `master` : le widget maître, souvent la fenêtre
- `listvariable` : une variable de type `StringVar` contenant la liste des mots à afficher, séparés par des espaces. Pour en savoir un pleu plus sur les `StingVar`, consulter la section [Les boutons à cocher et les boutons radio](bonus-checkbox.md). Si vous souhaitez que l'une des lignes de la `Listbox` contienne un espace, encadrez-la par des guillemets différents (simples ou doubles)
- `width`, `height` : indiquent la largeur et la hauteur de la `Listbox`. Attention, pour une fois ces valeurs ne sont pas données en pixels mais en caractères ! Ainsi `height = 5` affichera 5 lignes et `width = 8` limitera ces lignes à 8 caractères
- `activestyle` : règle l'apparence d'une ligne quand elle est sélectionnée. Elle peut être soulignée avec `activestyle = "underline"` (par défaut), encadrée avec `activestyle = "dotbox"` et neutre avec `activestyle = "none"`
- `selectmode` : permet de sélectionner une seule ligne (avec `"browse"` ou `"single"`) ou plusieurs (avec `"multiple"` ou `"extended"`). Cet attribut gère également le glisser-déposer. Un clic long ne change rien avec `"single"` et `"multiple"` alors qu'il sera actif avec `"browse"` et `"extended"`. 

!!! example "Exemple"

	```python
	choix = StringVar(master = fenetre, value = "tomates 'haricots verts' radis")
	ma_liste = Listbox(
		master = fenetre, 
		listvariable = choix,
		activestyle = 'none', 
		selectmode = 'single')
	```

Les `Listbox` disposent de nombreuses méthodes mais voici celles qui vous seront les plus utiles :

- `size()` : renvoie le nombre de lignes de la `Listbox`
- `curselection()` : renvoie un tuple (éventuellement vide) contenant les numéros des lignes sélectionnées (en commençant à 0 bien entendu)
- `get(n)` : renvoie le texte de la ligne numéro `n`
- il existe également des méthodes pour modifier la liste comme `set()`, `insert()` ou `delete()` mais elles ne devraient pas vous être utiles. 

!!! example "Exemple"

	```python
	def afficher(une_liste):
		print("La liste contient {0} éléments".format(une_liste.size()))
		for i in une_liste.curselection():
			print("{0} : {1}".format(i, une_liste.get(i)))

	mon_bouton = Button(fenetre, text = "Informations", command = lambda:afficher(ma_liste))
	```

Pour en savoir plus, [consulter la documentation](http://tkinter.fdex.eu/doc/lbw.html).

# Les menus déroulants

![Menu déroulant](pics/screen-combobox.png)

Les menus déroulants sont appelés `Combobox` et sont accessibles grâce à la sous-bibliothèque `tkinter.ttk`. Bien que ressemblant, leur fonctionnement est différent de celui des `Listbox`. Poru le construire, vous devrez spécifier les attributs suivants : 

- `master` : le widget maître comme toujours
- `values` : une tuple contenant les valeurs à afficher
- `textvariable` : si vous le souhaitez, vous pouvez lui attribuer une variable de type `StringVar`. L'idéal est alors de donner une valeur initiale à cette variable qui peut même être un message du type `"Choisissez une option"`

!!! example "Exemple"

	```python
	from tkinter.ttk import Combobox

	choix = StringVar(master = fenetre, value = "Choisissez une lettre")
	ma_combobox = Combobox(fenetre, values = ('A', 'B', 'C'), textvariable = choix)
	```

Pour accéder à l'unique valeur choisie par l'utilisateur, vous pourrez ensuite :

- soit passer par la variable de type `StringVar`
- soit utiliser la méthode `get()`

!!! example "Exemple"

	*Avec la variable de type `StringVar`*

	```python
	def afficher(variable):
		print(variable.get())

	mon_bouton = Button(fenetre, text = "Cliquez ici", command = lambda:afficher(choix))
	```
	*Sans la variable*

	```python
	def afficher(une_combobox):
		print(une_combobox.get())

	mon_bouton = Button(fenetre, text = "Cliquez ici", command = lambda:afficher(ma_combobox))
	```

# Les arbres

![Arbre](pics/screen-treeview.png) ![Arbre hiérarchique](pics/screen-treeview2.png)

Dans la bibliothèque `tkinter.ttk` vous trouverez un widget `Treeview` ressemblant à une `Listbox` à plusieurs colonnes, hiérarchisée sous forme d'arborescence. Par défaut, il existe toujours une première colonne portant le nom `"#0"` et où apparaîtront les icônes pour dérouler ou refermer l'arborescence. Pour ajouter d'autres colonnes, vous devrez compléter l'attribut `columns` par un tuple dont chaque composante permettra d'identifier la colonne. 

!!! example "Exemple"

	```python
	mon_arbre = Treeview(
		master = fenetre, 
		columns = (1, 2)
		)
	```

Si vous avez ajouté des colonnes (ce qui n'est pas obligatoire), vous pourrez leur attribuer un nom avec la méthode `heading()` ou les configurer avec `column()`. 

!!! example "Exemple"

	```python
	mon_arbre.heading("#0", text = "Nom", anchor = W)
	mon_arbre.heading(1, text = "Taille")
	mon_arbre.heading(2, text = "Type", anchor = E)

	mon_arbre.column("#0", width = 100, minwidth = 100)
	mon_arbre.column("1", width = 100, minwidth = 80)
	mon_arbre.column("2", width = 200)
	```

!!! warning "Remarque"

	Vous pouvez ne pas afficher les noms de colonne en ajoutant un attribut `show = "tree"` à la création de votre `Treeview`. Inversement, vous pouvez n'afficher que les colonnes ajoutées (et pas la `"#0"`) en ajoutant `show = "headings"`.*

Voyons maintenant comment ajouter une ligne à notre arbre. Nous allons utiliser une fonction `insert()` mais son usage est un peu complexe. Elle prend plusieurs attributs : 

- `parent` : l'identifiant de la ligne mère. Au début, cet attribut sera la chaîne de caractères vide `""`
- `index` : l'indice où cette ligne doit être ajoutée (0 pour l'ajouter au début, `END` pour l'ajouter à la fin)
- `iid` : l'identifiant de votre insertion, prenez une chaîne de caractères courtes et compréhensible
- `text` : la valeur qui apparaîtra dans la colonne `"#0"`
- `values` : un tuple permettant de compléter les autres colonnes (inutile si vous n'avez pas ajouté de colonnes)

!!! example "Exemple"

	```python linenums='1'
	#Lignes de niveau 1
	mon_arbre.insert(
		parent = "", index = END, iid = "ligne1", 
		text = "Mes images", 
		values = ("1,5 Mo", "Dossier"))
	mon_arbre.insert("", END, "ligne2", "Mes vidéos", 
		values = ("0 ko", "Dossier"))

	#Lignes de niveau 2
	mon_arbre.insert(
		parent = "ligne1", index = END,
		text = "Ibiza.jpg", 
		values = ("1,2 Mo", "Fichier image"))
	mon_arbre.insert(
		parent = "ligne1", index = END,
		text = "icone.png", 
		values = ("300 ko", "Fichier image"))
	```

Enfin, accéder aux éléments sélectionnés s'avère un peu complexe. On y accède en écrivant `mon_arbre.item()`, mais celui-ci prend deux attributs : 

- `item` : l'identifiant de la ligne désirée. Pour obtenir les identifiants sélectionnés, la méthode `mon_arbre.selection()` vous sera utile
- `option` : l'élément désiré. Cet attribut peut valoir `text` ou `values`

!!! example "Exemple"

	```python
	print(mon_arbre.item(item = "ligne1", option = "text"))

	for elt in mon_arbre.selection():
		print(mon_arbre.item(elt, 'values'))
	```