---
title: Les boîtes de sélection
author: M.Jarc
tags:
    - Tkinter
---

# Les boîtes de sélection de valeur

Tkinter vous permet d'ouvrir une boîte de dialogue pour saisir un nombre entier, un nombre flottant ou une chaîne de caractère grâce aux fonctions `askinteger()`, `askfloat()` et `askstring()`. Celles-ci prennent deux paramètres : 

- `title` : pour le titre de la boîte de sélection,
- `prompt` : le message à afficher. 

Ces fonctions renvoient un résultat qui aura nécessairement le bon type (avec `askinteger()`, l'utilisateur ne pourra pas écrire de texte par exemple). 

!!! example "Exemple"

	```python
	nombre = askinteger(title = "Saisie", prompt = "Entrez un nombre entier")
	print(nombre)
	```

# La boîte de sélection de couleur

Pour utiliser les boîtes de sélection de couleur, vous devez commencer par importer une sous-bibliothèque : `from tkinter.colorchooser import *`. Ce module contient une seule fonction `askcolor()`. Celle-ci prend un paramètre `title`.

Elle renvoie en résultat un tuple de la forme `((21, 255, 147), '#15ff93')`. La première valeur de ce tuple est lui-même un tuple correspondant aux valeurs RGB (*Red Green Blue*) de la couleur choisie ; la seconde valeur correspond à la même valeur, donnée sous forme hexadécimale. Si aucune couleur n'a été choisie, la fonction renverra `None`.

!!! example "Exemple"

	```python
	couleur = askcolor(title = "Choisir la couleur de fond")
	if couleur is not None :
		fenetre.config(bg = couleur[1])
	```