---
title: Mettre une image dans un bouton
author: M.Jarc
tags:
    - Tkinter
---

# Une image à la place du texte

Pour créer un bouton avec une icone plutôt que du texte, vous devez d'abord créer une `PhotoImage` (voir la section [Les PhotoImage](bonus-photoimage.md)) puis, à la création du `Button` vous devez utiliser le mot-clé `image` plutôt que `text`. 

!!! example "Exemple"

	```python
	img_quelconque = PhotoImage(file = 'nomDuFichier.png')
	btn_quelconque = Button(master = fenetre, image = img_quelconque)
	```

# Une image et un texte

Si vous souhaitez avoir une image et un texte, vous allez devoir comment les organiser. Utilisez pour cela le mot-clé `compound` suivi de `LEFT`, `RIGHT`, `TOP` ou `BOTTOM`. 

!!! example "Exemple"

	```python linenums='1'
	img_quelconque = PhotoImage(file = 'nomDuFichier.png')
	btn_quelconque = Button(
		master = fenetre, 
		text = "Un texte quelconque", 
		image = img_quelconque, 
		compound = LEFT)
	```