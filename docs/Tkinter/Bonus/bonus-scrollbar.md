---
title: Les ascenseurs
author: M.Jarc
image: "./pics/title0.png"
tags:
	- Tkinter
---

![ascenseurs](pics/screen-scrollbar.png)

Les **ascenseurs** ou **barres de défilement** se nomment `Scrollbar` en Tkinter. Elle comporte les habituels attributs `width`, `background` ou `relief`, mais le plus important est `orient` qui définira si votre ascenseur est horizontal ou vertical : 

!!! example "Exemple"

    ```python
    editeur_texte = Text(fenetre, width = 50, height = 20, wrap = 'none')
    scroll_x = Scrollbar(fenetre, orient = HORIZONTAL)
    scroll_y = Scrollbar(fenetre, orient = VERTICAL)

    editeur_texte.grid(row = 0, column = 0)
    scroll_x.grid(row = 1, column = 0, sticky = EW) #On étire la barre horizontale à gauche et à droite
    scroll_y.grid(row = 0, column = 1, sticky = NS) #On étire la barre verticale en haut et en bas
    ```

Pour que vos ascenseurs fonctionnent, encore faut-il qu'ils soient connectés au widget qu'ils sont sensés contrôler. Attention, cette connexion se fait dans les deux sens : des ascenseurs vers le widget et réciproquement. Pour vos ascenseurs, cela se fait en complétant leur attribut `command` avec la méthode `xview()` ou `yview` de votre widget. Pour votre widget, il faudra configurer ses attributs `xscrollcommand` et `yscrollcommand` avec les méthodes `set` de vos ascenseurs. 

!!! example "Exemple"

    ```python
    editeur_texte = Text(fenetre, width = 50, height = 20, wrap = 'none')
    scroll_x = Scrollbar(fenetre, orient = HORIZONTAL, command = editeur_texte.xview)
    scroll_y = Scrollbar(fenetre, orient = VERTICAL, command = editeur_texte.yview)
    editeur_texte.config(xscrollcommand = scroll_x.set, yscrollcommand = scroll_y.set)
    ```

!!! warning "Remarques"

    - Attention, tous les widgets n'ont pas la possibilité d'être "scrollés". Les consignes précédentes s'appliquent entre autres aux `Canvas`, `Text`, `Entry`, `Listbox`, `Treeview` et `Combobox`.*
    - Pour pouvoir faire défiler un `Canvas`, il faudra configurer sa taille : `width` et `height` indiqueront sa taille apparente et `scrollregion` indiquera sa véritable taille. Par exemple : `scrollregion=(0,0,1599,1199)` indique que le `Canvas` aura une largeur de 1600px et une hauteur de 1200px. 
    - Lors d'un clic de souris sur un `Canvas`, vous pouvez connaître les coordonnées du clic avec `nom_evenement.x` et `nom_evenement.y` (voir la section sur [les événements](bonus-evenements.md)). Ces coordonnées sont celles du clic dans la fenêtre. Pour connaître les coordonnées dans le `Canvas`, il faudra utiliser les commandes `nom_canvas.canvasx(nom_evenement.x)` et `nom_canvas.canvasy(nom_evenement.y)`. 

Pour en savoir plus, [consultez la documentation en Français](http://tkinter.fdex.eu/doc/scrw.html).