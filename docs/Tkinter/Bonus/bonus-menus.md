---
title: Les barres de menu
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

![Menu](pics/screen-menubar.png)

# Créer un menu

L'élaboration d'une barre de menu est plutôt fastidieuse. Voyons la première étape : la création de la barre de menus grâce au widget `Menu`. Le principal attribut est le paramètre `master`. Je vous invite également à ajouter systématiquement l'attribut `tearoff = 0` à vos menus et sous-menus pour éviter qu'ils ne soient "*détachables*". Enfin, pour ajouter votre `Menu` à la fenêtre, il n'est pas question d'utiliser `pack()` ou `grid()` ! Vous devez configurer l'attribut `menu` de votre fenêtre. 

!!! example "Exemple"

    ```python
    barre_de_menu = Menu(master = fenetre, tearoff = 0)
    fenetre.config(menu = barre_de_menu)
    ```

# Ajouter des commandes

Vous devez ensuite peupler votre `Menu`. Le plus simple est d'y ajouter des **commandes** soit avec la méthode `add_command()` (elle sera ajoutée à la fin) soit avec `insert_command()`. Leurs principaux paramètres sont :

- `label` : (`str`) le texte qui sera affiché
- `image` : (`Photoimage`) l'image qui sera affichée
- `command` : le nom de la fonction qui sera exécutée lors du clic (vous pourrez utiliser des fonctions lambda si besoin)

Il s'agit en définitive d'ajouter un simple bouton. 

!!! example "Exemple"

    ```python
    barre_de_menu.add_command(label = 'Quitter', command = fenetre.destroy)
    barre_de_menu.insert_command(index = 0, label = 'Ouvrir') #On insère la commande au début
    ```

De manière assez similaire, vous pouvez ajouter ou insérer des cases à cocher, des boutons radio et des séparateurs avec les fonctions `add_checkbutton()`, `add_radiobutton()`, `add_separator()`, `insert_checkbutton()`, `insert_radiobutton()` et `insert_separator()`. Pour les cases à cocher et les boutons radio, vous aurez besoin des attributs `label` et `variable` alors qu'aucun attribut ne sera nécessaire pour les séparateurs. Je vous invite à lire auparavant la section [Les boutons à cocher et les boutons radio](bonus-checkbox.md). 

# Ajouter des sous-menus

Mais lorsque l'on crée des barres de menu, ce que l'on souhaite avant tout, c'est créer des menus déroulants, des sous-menus. Le principe en Tkinter est assez simple : chaque menu déroulant est lui-même un `Menu` ! L'ajout se fait grâce à la méthode `add_cascade()` ou `insert_cascade()`. 

!!! example "Exemple"

    ```python linenums='1'
    barre_de_menu = Menu(master = fenetre, tearoff = 0)

    menu_fichier = Menu(master = barre_de_menu, tearoff = 0)
    menu_fichier.add_command(label = 'Ouvrir')
    menu_fichier.add_separator()
    menu_fichier.add_command(label = 'Quitter')

    barre_de_menu.add_cascade(menu_fichier)
    ```

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/menw.html). 

# Les boutons menus

Il existe également un widget `Menubutton` permettant de créer un bouton qui déroulera un menu lorsque l'on cliquera dessus. Celui-ci se configure à la manière d'un bouton classique à ceci près qu'il dispose d'un attribut `menu` ainsi que d'un attribut `direction` pour indiquer dans quelle sirection s'ouvrira le menu déroulant (les valeurs possibles étant `below`, `above`, `right` ou `left`). 

!!! example "Exemple"

    ```python
    mon_bouton_menu = Menubutton(master = fenetre, text = "cliquez ici", menu = menu_fichier)
    mon_bouton_menu.config(direction = 'above')
    ```

Pour en savoir plus, [cliquez ici](http://tkinter.fdex.eu/doc/menbw.html). 