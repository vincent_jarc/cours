---
title: Deux fenêtres pour une application
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

Si jamais vous aviez besoin d'ouvrir une seconde fenêtre dans votre application qui ne soit pas une simple boîte de dialogue, alors je vous déconseille d'ouvrir une deuxième fenêtre de type `Tk`. Vous seriez gênés par la gestion des événements, seule l'une des deux pouvant exécuter son `mainloop()`. Au lieu de cela, Tkinter vous propose des widgets de type `Toplevel`. Vous retrouverez les attributs habituels comme `bd`-`borderwidth`, `bg`-`background`, `height`, `width`, `relief`... vous trouverez également l'attribut `menu` pour créer des menus spécifiques à votre fenêtre. 

Côté méthodes, vous retrouverez les habituels `title()`, `resizable()` ou `geometry()`. Plus important, vous trouverez une fonction `transient()` prenant un paramètre `parent` correspondant à la fenêtre mère. Cette fonction permettra que votre fenêtre `Toplevel` soit toujours positionnée devant la fenêtre mère. Elle se réduira ou réapparaîtra également en même temps que sa fenêre mère. 

Autre méthode utilile : `overrideredirect()`. Appelée avec l'argument `True`, la fenêtre ne pourra plus être modifiée par l'utilisateur (ni redimensionnée, ni réduite, ni fermée). Appelée avec l'argument `False`, cela rétablit le comportement normal de la fenêtre. Attention, cette méthode peut ne pas marchersous certains OS. 

!!! example "Exemple"

	```python linenums='1'
	def creer_fenetre(master):
		nouvelle_fenetre = Toplevel(master)
		message = Label(nouvelle_fenetre, text = "coucou")
		message.pack()
		bouton = Button(nouvelle_fenetre, text = "Quitter", command = nouvelle_fenetre.destroy)
		bouton.pack()
		nouvelle_fenetre.transient(master)
		nouvelle_fenetre.overrideredirect(True)

	fenetre = Tk()
	bouton_unique = Button(
		fenetre, 
		text = "Ouvrir une fenêtre", 
		command = lambda : creer_fenetre(fenetre) 
		)
	bouton_unique.pack()
	fenetre.mainloop()
	```

Vous pouvez décider de mettre cette fenêtre à l'avant-plan avec la méthode `lift()` ou à l'arrière-plan avec `lower()`. Enfin, votre fenêtre principale peut attendre que votre seconde fenêtre soit fermée pour entreprendre une autre action, grâce à la méthode `wait_window()` : `fenetre.wait_window(nouvelle_fenetre)`. 

Pour en savoir plus, [consultez la documentation en Français](http://tkinter.fdex.eu/doc/toplww.html).