---
title: Les onglets
author: M.Jarc
image: "./pics/title0.png"
tags:
    - Tkinter
---

![Onglets](pics/screen-notebook.png)

# Création de la boîte

La boîte à onglets est appelée `Notebook` dans tkinter mais ce n'est pas un widget disponible nativement avec la bibliothèque `tkinter`, il faut faire appel au sous-module `tkinter.ttk`. Il s'agit d'un conteneur, comme les `Frame` et les `PanedWindow` (voir la section [Les conteneurs : frames et panneaux](bonus-conteneurs.md)) donc ses principaux attributs sont `width` et `height`. 

!!! example "Exemple"

    ```python
    from tkinter.ttk import Notebook

    fenetre = Tk()
    boite_onglet = Notebook(master = fenetre, height = 200)
    boite_onglet.pack()
    ```

# Ajout d'onglet

Une fois la boîte à onglet créée, reste à ajouter des onglets et du contenu. Cela se fait avec la méthode `add()` dont les paramètres sont : 

- `self` : votre boîte à onlets,
- `child` : le widget qui sera contenu sous l'onglet (en général, ce sera un conteneur comme `Frame`), 
- `text` : le texte qui sera affiché dans l'onglet

!!! warning "Remarque"

    Il est possible d'ajouter une image à l'onglet, à la place ou en plus du texte. Pour cela, voir la section [Mettre une image dans un bouton](bonus-bouton-image.md).

!!! example "Exemple"

    ```python
    ma_frame1 = Frame(boite_onglet)
    ma_frame1.pack()
    ma_frame2 = Frame(boite_onglet)
    ma_frame2.pack()
    boite_onglet.add(ma_frame1, text = "le premier onglet")
    boite_onglet.add(ma_frame2, text = "le deuxième onglet")
    ```

# Cacher un onglet

Il est possible de cacher temporairement un onglet avec `hide()` puis de le refaire apparaître avec `add()`. Pour le supprimer définitivement, vous pourrez utiliser la fonction `forget()`

!!! example "Exemple"

    ```python
    boite_onglet.hide(0) #cache le premier onglet
    ```