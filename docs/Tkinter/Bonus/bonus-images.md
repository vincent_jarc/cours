---
title: "Gestion des fichiers images"
author: M.Jarc
tags:
    - Tkinter
---

# Le format

Les `PhotoImage` supportent les formats PGM, PPM, GIF et PNG. Je vous conseille de privilégier le PNG. Ce format permet de gérer les couleurs sur 24 bits plutôt qu'avec une palette, il gère la transparence et est compressé. Son mécanisme de compression le rend efficace pour des images telles les icônes mais beaucoup moins pour des photos (le JPEG serait plus adapté). 

Si vous aviez besoin de manipuler d'autres formats comme le JPEG, il est possible de combiner les `PhotoImage` avec la bibliothèque `Pillow`. [Quelques exemples ici](https://www.activestate.com/resources/quick-reads/how-to-add-images-in-tkinter/). 


# La taille des images

Il est possible de redimensionner ou zoomer une PhotoImage. Mais je vous le déconseille, il est préférable que vous images soient tout de suite à la bonne taille. Éditez-les avec des logiciels comme Gimp, Krita ou simplement Paint pour les redimensionner ou les retravailler. Un conseil : privilégiez des dimensions qui soient des puissances de 2 (32 pixels ou 256 pixels) pour pouvoir aisément les diviser par 2. 

!!! example "Exemple" 
    
    Une carte peut être réalisée à l'aide d'une grille d'images carrées de 64x64px. Les éléments positionnés par dessus ces tuiles auront eux aussi une dimension de 64x64px.

# La transparence

Le PNG gère la transparence. Il est possible de rendre transparents certains pixels de la PhotoImage, mais le mieux est encore que cette transparence soit gérée au niveau des fichiers. Si vous utilisez des logiciels comme Gimp ou Krita, les zones transparentes apparaissent sous la forme de damiers blancs et gris. 