---
title: "Les conteneurs : frames et panneaux"
author: M.Jarc
tags:
    - Tkinter
---

# Principe

Un *conteneur* est un widget prévu pour contenir d'autres widgets de toutes sortes. Votre fenêtre est le conteneur de base. Mais vous remarquerez rapidement qu'il est préférable de scinder le travail sur l'interface en plusieurs morceaux. Pour rendre votre travail moins dense et plus cohérent tout d'abord, pour permettre le partage du travail au sein du groupe par la suite. 

# Les frames

![Frame](pics/screen-frame.png)

Les frames ou `Frame` apparaissent comme de simples rectangles. Vous retrouverez la plupart des mots-clés habituellement disponibles, notamment ceux rencontrés pour les fenêtres. Ajoutons `padding` et `margin` qui permettent de réaliser une marge à l'intérieur ou à l'extérieur du bord de la `Frame`. L'ajout de widgets dans une frame se fait de la même manière que dans une fenêtre, avec `pack()`, `grid()` ou `place()`. 

!!! example "Exemple"

    ```python
    ma_frame = Frame(master = fenetre, padding = 2)
    ma_frame.grid(row = 0, column = 0)

    mon_bouton = Button(master = ma_frame, text = "Cliquez ici")
    mon_bouton.grid(row = 0, column = 0)
    ```

Vous remarquerez deux choses : 

- tout d'abord, le `master` de `mon_bouton` n'est plus `fenetre` mais `ma_frame`. Ainsi, détruire la `Frame` entraînera la destruction immédiate du `Button`. 
- ensuite, `ma_frame` et `mon_bouton` sont tous deux positionnés aux mêmes coordonnées. Et pour cause : `ma_frame` est à la première ligne et première colonne de la `fenetre`, alors que `mon_bouton` est à la première ligne et première colonne de `ma_frame`. 

# Les panneaux

![Panneau](pics/screen-paned.gif)

Les panneaux ou `PanedWindow` ont pour objectif de scinder la fenêtre en plusieurs zones que l'utilisateur pourra redimensionner si besoin, chaque zone ne contenant qu'un seul widget (ce widget peut être un autre conteneur si vous souhaitez mettre plusieurs widgets). Le principal paramètre des panneaux est `orient` qui indique si les zones sont arrangées horizontalement ou verticalement. Ses valeurs sont donc `HORIZONTAL` et `VERTICAL`. Vous retrouverez bien sûr les attributs habituels comme `borderwidth`, `background`, `height`, `width` ou `relief`. 

!!! example "Exemple"

    A l'empaquetage, mieux vaut occuper toute la place disponible.

    ```python
    mon_panneau = PanedWindow(master = fenetre, orient = HORIZONTAL)
    mon_panneau.pack(expand = True, fill = BOTH)
    ```

Pour ajouter un widget à un panneau, vous devrez utiliser la command `add()` et non `pack()` ou `grid()`. Si vous avez choisi une orientation horizontale, le premier widget ajouté sera à gauche, si vous avez choisi une orientation verticale, le premier widget ajouté sera en haut. 

!!! example "Exemple"

    On crée des boutons dont le maître sera `mon_panneau`.

    ```python
    bouton1 = Button(master = mon_panneau, text = "bouton gauche")
    bouton2 = Button(master = mon_panneau, text = "bouton droit")
    mon_panneau.add(bouton1)
    mon_panneau.add(bouton2)
    ```

Si vous souhaitez paramétrer la ligne de séparation entre les différentes zones, vous devrez configurer votre panneau à l'aide des attributs suivants : 

- `sashrelief` : indique le relief de la ligne de séparation. Prend les mêmes valeurs que `relief`, c'est-à-dire `RAISED`, `SUNKEN`, `FLAT`... la valeur par défaut étant `FLAT`. 
- `sashwidth` (`int`) : indique la largeur de la ligne de séparation. 
- `showhandle` (`bool`) : permet d'afficher une poignée de saisie pour mieux visualiser la ligne de séparation. 
- `handlesize` (`int`) : indique la largeur de la poignée de saisie. 

Si besoin, vous trouverez [plus d'informations ici](http://tkinter.fdex.eu/doc/panww.html). 