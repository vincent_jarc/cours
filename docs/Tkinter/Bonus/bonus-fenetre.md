---
title: Paramétrer la fenêtre
author: M.Jarc
tags:
    - Tkinter
---

# Définir la taille de la fenêtre

Une première méthode pour modifier les dimensions de votre fenêtre est de la reconfigurer avec `fenetre.config(width = ..., height = ...)`. 

Une autre méthode peut être utilisée et est souvent employée dans les forums `geometry(self, newGeometry)`. Le paramètre `self` correspond à votre fenêtre et le paramètre `newGeometry` est une chaîne de caractère indiquant la nouvelle configuration de la fenêtre. Cette chaîne de caractère a la forme `"largeurxhauteur+abscisse+ordonnée"`.

On peut même connaître la géométrie actuelle de la fenêtre en ne renseignant pas de paramètres. 

!!! example "Exemple"

    ```python
    #Une fenêtre de 300px de large, 200px de haut, situé en (100, 0)
    fenetre.geometry("300x200+100+0")
    #Une fenêtre de 600px de large, 400px de haut
    fenetre.geometry("600x400")
    #Affiche la géométrie de la fenêtre
    print(fenetre.geometry())
    ```

Il est également possible d'empêcher que la fenêtre soit redimensionnable avec la méthode `resizable()`. Celle-ci prend deux booléens en paramètre, le premier indique s'il est possible de redimensionner la largeur de la fenêtre, le second la hauteur. 

!!! example "Exemple"

    ```python
    fenetre.resizable(False, False) #Impossible de redimensionner
    fenetre.resizable(True, False)  #Impossible de redimensionner en hauteur
    ```

# Fenêtre en plein écran

Pour mettre la fenêtre en plein écran, il faut utiliser la méthode `attributes()` mais celles-son utilisation dépendra si vous êtes sous Windows ou pas. 

!!! example "Exemple"

    ```python
    # Sous Windows
    fenetre.attributes("-fullscreen", True)
    # Sous Ubuntu
    fenetre.attributes("-zoomed", True)
    ```

Utiliser ces fonctionnalités feront que votre programme ne sera plus multiplateforme. Une autre solution, moins aboutie, consiste à récupérer la largeur et la hauteur de votre écran et à dimensionner la fenêtre en conséquence. 

!!! example "Exemple"

    ```python
    largeur = fenetre.winfo_screenwidth()
    hauteur = fenetre.winfo_screenheight()
    # OU
    largeur, hauteur = fenetre.maxsize()
    fenetre.config(width = largeur, height = hauteur)
    ```

# Icone de fenêtre

Il est possible d'attribuer une icône à votre fenêtre et à ses descendants grâce à la méthode `iconbitmap(self, bitmap = None, default = None)`. Les paramètres sont : 

- `self` : votre fenêtre, 
- `bitmap` : (type `str`) chemin pour accéder au fichier .ICO, 
- `default` : (type `str`) similaire à bitmap mais l’icône sera automatiquement attribué aux descendants de la fenêtre.

!!! example "Exemple"

    ```python
    fenetre.iconbitmap(bitmap = "./icons/sword.ico")
    ```

!!! danger "Attention"
    
    Cette fonction ne prend pas n'importe quel type de fichier image, seulement les fichiers .ICO ! 