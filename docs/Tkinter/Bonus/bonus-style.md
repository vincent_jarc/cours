---
title: Modifier le style des widgets
author: M.Jarc
image: "./pics/title0.png"
tags:
	- Tkinter
---

# Les thèmes préexistants

Si l'allure de votre fenêtre ne vous plaît pas, sachez qu'il existe d'autres thèmes avec Tkinter. Mais avant cela, vous devez créer un objet de type `Style()` (disponible grâce à la bibliothèque `tkinter.ttk`) puis utiliser la méthode `theme_names()` pour obtenir la liste des thèmes disponibles ou la méthode `theme_use()` pour connaître le thème actuel.

!!! example "Exemple"

	```python
	from tkinter.ttk import Style
	from tkinter import Tk

	fenetre = Tk()
	style = Style()
	print(style.theme_names())
	print(style.theme_use())
	fenetre.mainloop()
	```

Tkinter devrait vous proposer les thèmes **clam**, **alt**, **default** et **classic**. Ensuite, cela dépend de votre OS : sous Windows vous aurez également le choix entre **winnative**, **vista** et **xpnative** ; sous MacOS, le thème est **aqua**. Pour les tester, je vous invite à créer une fenêtre avec de nombreux widgets, puis à  utiliser `theme_use()` en indiquant en paramètre le thème voulu. 

!!! example "Exemple"

	```python
	style.theme_use('clam')
	```

# Modifier un style existant

Les objets de type `Style` permettent d'enregistrer les paramètres de style. Vous pouvez donc les modifier et ainsi configurer l'apparence votre interface. Cela se fait avec la méthode `configure()` (attention, `config()` n'existe pas) en indiquant le nom du style à modifier suivi des attributs habituels comme `font`, `background`, `foreground`, `borderwidth`, `width` ou `padding`... Attention, vous nez pourrez pas configurer les widgets de la bibliothèque `tkinter` mais ceux issus de la bibliothèque `tkinter.ttk` (qui contient des doubles de bon nombre de widgets). 

!!! example "Exemple"

	Le style des `Button` est `"TButton"`

	```python
	from tkinter.ttk import Button #IMPORTANT !!!

	style.configure("TButton", font="{Courier New} 14 bold italic", foreground = "red")
	```

Les styles des widgets sont assez simple à connaître puisque ces chaînes de caractères s'écrivent avec un T majuscule devant le nom de leur widget. Le style des `Label` est ainsi `"TLabel"`. Il y a toutefois quelques exceptions : 

- le style des `Treeview` s'appelle `"Treeview"` (sans T supplémentaire)
- il faut précéder les style des `Progressbar`, `Scale` et `Scrollbar` de `"Horizontal."` ou `"Vertical."`. Par exemple `"Horizontal.TProgressbar"` ou `"Vertical.TScrollbar"`. 

# Compléter un style

Si vous souhaitez que certains boutons aient une apparence particulière mais pas tous les boutons, vous pouvez également créer de nouveaux styles. La convention veut que vous gardiez le nom d'origine du style en le précédant du mot de votre choix (par exemple `"Perso.TButton"`). La configuration de ce style se fera comme précédemment mais vous devrez préciser le `style` de votre `Button` à sa création. 

!!! example "Exemple"

	```python
	style.configure("Perso.TButton", font="{Courier New} 14 bold italic", foreground = "red")
	mon_bouton = Button(
		fenetre,
		text = "clicquez ici",
		style = "Perso.TButton")
	```

# Pour quelques thèmes de plus

Vous trouverez sur Internet des thèmes complet pour Tkinter (comme [ce site](https://rdbende.github.io/tkinter-docs/tutorials/how-to-use-themes.html)) qui seront plus attrayants que les thèmes par défaut. Voici une méthode clé en main :

- Rendez-vous sur le [site suivant](https://rdbende.github.io/tkinter-docs/resources/list-of-ttk-themes.html) et cliquez sur le bouton **View on Github**.
- Une fois sur Github, vous aurez besoin de télécharger les fichiers proposés. Pour cela, revenez au dossier `ttkthemes` puis cliquez sur le bouton **Code** et enfin sur **Download ZIP**. 
- Ouvrez le fichier ZIP téléchargé, entrez par exemple dans le dossier `ttkthemes/png/adapta`. 
- Copiez le fichier `adapta.tcl` et le dossier `adapta` et collez-les au même emplacement que votre projet. 
- Créez une interface graphique et ajoutez les lignes de code suivantes : 

```python
fenetre.tk.call("source", "adapta.tcl")
style.theme_use("adapta")
```

Vous trouverez également quelques autres thèmes [ici](https://github.com/RobertJN64/TKinterModernThemes) ainsi que des bibliothèques complètes avec un éditeur de thème [ici](https://pythonawesome.com/a-collection-of-modern-themes-for-tkinter-ttk/). 