---
author: M.JARC
title: "Projet"
tags:
  - projet
---

# Règles

- Le travail se fera par groupes de 3 à 4 élèves et sera découpé en phases de 4 à 5 semaines
- Un cahier des charges sera distribué en début de chaque phase afin d'expliquer les objectifs à atteindre et de préciser les différentes tâches. 
- Chaque tâche sera détaillée sur Gitlab dans la section `Programmation/Ticket` et permettra d'obtenir des points d'expérience. 
- Une séance de Hackathon portant sur le projet sera organisée au mieux une semaine sur deux à la suite des tests express. Un travail personnel sera toutefois nécessaire. 
